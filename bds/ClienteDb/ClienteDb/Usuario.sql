﻿CREATE TABLE [dbo].[Usuario]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Nome] NVARCHAR(50) NULL, 
    [Senha] NVARCHAR(20) NULL, 
    [CodigoTipoUsuario] TINYINT NOT NULL, 
    [Email] NVARCHAR(50) NOT NULL, 
    [Telefone] NVARCHAR(50) NOT NULL
)
