﻿CREATE TABLE [dbo].[HistoricoStatusEntrega]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [IdPedido] INT NOT NULL, 
    [StatusAtual] INT NOT NULL, 
    [IdStatusAnterior] INT NULL, 
    [NomeTransportadora] NVARCHAR(50) NULL, 
    [CodigoRastreio] NVARCHAR(50) NULL, 
    [UrlRastreio] NVARCHAR(MAX) NULL, 
    [DataModificacao] DATETIME NOT NULL, 
    [Observacao] NVARCHAR(MAX) NULL, 
    CONSTRAINT [FK_HistoricoStatusEntrega_HistoricoStatusEntrega] FOREIGN KEY ([Id]) REFERENCES [HistoricoStatusEntrega]([Id])
)
