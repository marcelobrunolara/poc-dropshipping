﻿CREATE TABLE [dbo].[PedidoCliente]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [IdCliente] VARCHAR(50) NULL, 
    [Total] MONEY NULL, 
    [Data] DATETIME NULL
)
