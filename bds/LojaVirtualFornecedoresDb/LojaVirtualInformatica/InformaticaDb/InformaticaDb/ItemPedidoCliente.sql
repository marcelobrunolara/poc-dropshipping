﻿CREATE TABLE [dbo].[ItemPedidoCliente]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [IdPedidoCliente] INT NULL, 
    [IdProduto] INT NULL, 
    [PrecoProduto] MONEY NULL
)
