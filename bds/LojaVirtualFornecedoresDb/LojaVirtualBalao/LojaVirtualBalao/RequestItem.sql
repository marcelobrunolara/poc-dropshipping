﻿CREATE TABLE [dbo].[RequestItem]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [RequestId] INT NULL, 
    [ProductId] NVARCHAR(50) NULL
)
