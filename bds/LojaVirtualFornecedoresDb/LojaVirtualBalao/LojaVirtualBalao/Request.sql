﻿CREATE TABLE [dbo].[Request]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [TotalPrice] MONEY NULL, 
    [RequestDate] VARCHAR(50) NULL, 
    [UserId] VARCHAR(50) NULL 
)
