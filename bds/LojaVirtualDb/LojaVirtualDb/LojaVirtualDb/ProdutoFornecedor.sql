﻿CREATE TABLE [dbo].[ProdutoFornecedor]
(
	[Id] INT NOT NULL PRIMARY KEY, 
    [IdProduto] INT NOT NULL, 
    [IdFornecedor] INT NOT NULL, 
    [Preco] MONEY NULL DEFAULT 0, 
    [IdProdutoNoFornecedor] NVARCHAR(50) NOT NULL DEFAULT 0, 
    CONSTRAINT [FK_ProdutoFornecedor_ToProduto] FOREIGN KEY ([IdProduto]) REFERENCES [Produto]([Id]), 
    CONSTRAINT [FK_ProdutoFornecedor_ToFornecedor] FOREIGN KEY ([IdFornecedor]) REFERENCES [Fornecedor]([Id])
)
