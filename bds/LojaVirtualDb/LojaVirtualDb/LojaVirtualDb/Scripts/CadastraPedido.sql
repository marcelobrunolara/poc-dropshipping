﻿--cadastra dois pedidos em nome do cliente de id 1, feitos por vendedor externo

--pedido 1
INSERT INTO Pedido(IdCliente, IdVendedor, OrigemPedido, PrecoTotal, StatusEntrega, DataPedido)
VALUES (1, 1, 2, 2129, 1, GETDATE())

INSERT INTO ItemPedido(IdPedido, IdProdutoFornecedor, Preco)
VALUES (1, 5, 29.00) --mouse wireless - informática 2000

INSERT INTO ItemPedido(IdPedido, IdProdutoFornecedor, Preco)
VALUES (1, 6, 2100.00) -- notebook - informática 2000

--pedido 2
INSERT INTO Pedido(IdCliente, IdVendedor, OrigemPedido, PrecoTotal, StatusEntrega, DataPedido)
VALUES (1, 1, 2, 79.00, 1, GETDATE())

INSERT INTO ItemPedido(IdPedido, IdProdutoFornecedor, Preco)
VALUES (1, 1, 79.00) -- teclado wireless - balão da informática


select * from Pedido