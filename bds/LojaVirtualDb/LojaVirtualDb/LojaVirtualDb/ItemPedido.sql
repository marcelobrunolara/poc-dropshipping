﻿CREATE TABLE [dbo].[ItemPedido]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [IdProdutoFornecedor] INT NULL, 
    [Preco] MONEY NOT NULL DEFAULT 0, 
    [IdPedido] INT NOT NULL, 
    CONSTRAINT [FK_ItemPedido_ProdutoFornecedor] FOREIGN KEY ([IdProdutoFornecedor]) REFERENCES [ProdutoFornecedor]([Id])
)
