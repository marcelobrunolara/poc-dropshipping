﻿CREATE TABLE [dbo].[Produto]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Nome] NVARCHAR(50) NULL, 
    [Descricao] NVARCHAR(MAX) NULL, 
    [CodigoCategoria] SMALLINT NULL 
)
