﻿CREATE TABLE [dbo].[Fornecedor]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [NomeFornecedor] NVARCHAR(50) NULL, 
    [Endereco] NVARCHAR(50) NULL, 
    [Telefone] NVARCHAR(50) NULL
)
