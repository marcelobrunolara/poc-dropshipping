﻿CREATE TABLE [dbo].[Pedido]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [IdCliente] INT NOT NULL, 
    [PrecoTotal] MONEY NOT NULL DEFAULT 0, 
    [DataPedido] DATETIME NULL, 
    [StatusEntrega] SMALLINT NULL, 
    [OrigemPedido] SMALLINT NOT NULL, 
    [IdVendedor] INT NULL, 
    [IdFornecedor] INT NULL
)
