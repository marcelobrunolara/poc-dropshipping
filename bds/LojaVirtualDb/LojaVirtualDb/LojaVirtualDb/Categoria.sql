﻿CREATE TABLE [dbo].[Categoria]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Descricao] NVARCHAR(50) NULL
)
