﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LojaVirtualWebApi.Model;
using LojaVirtualWebAPi.Data;
using Microsoft.AspNetCore.Mvc;

namespace LojaVirtualWebAPi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProdutosController : ControllerBase
    {
        // GET api/values
        [HttpGet]
        public IEnumerable<ProdutoModel> Get([FromServices] ProdutoDAO produtoDAO)
        {
            return produtoDAO.ListarTodos();
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<ProdutoModel> Get([FromServices] ProdutoDAO produtoDAO, int id)
        {
            return produtoDAO.BuscarProdutoPorId(id);
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }


        [HttpGet("Check")]
        public ActionResult<string> Check(int id)
        {
            return "LojaVirtualWebApi está online";
        }
    }
}
