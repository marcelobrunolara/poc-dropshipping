﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LojaVirtualWebApi.Model;
using LojaVirtualWebAPi.Data;
using LojaVirtualWebAPi.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace LojaVirtualWebAPi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PedidosController : ControllerBase
    {
        // GET api/values
        [HttpGet]
        public IEnumerable<PedidoModel> Get([FromServices]PedidoDAO pedidoDAO)
        {
            return pedidoDAO.ListarTodos();
        }

        //// GET api/values
        [HttpGet("{Id}")]
        public PedidoModel Get([FromServices]PedidoDAO pedidoDAO, [FromServices]LogisticaService logisticaService, int id)
        {
            var pedido = pedidoDAO.BuscarPedido(id);

            if (pedido is null)
                return pedido;

            pedido.UltimoStatusEntrega = logisticaService.BuscarUltimoStatus(id);
            pedido.DescricaoStatusEntrega = pedidoDAO.BuscarDescricaoStatusEntrega(pedido.UltimoStatusEntrega.StatusAtual);

            return pedido;
        }

        //// GET api/values
        [HttpGet("SolicitarCompra/{Id}")]
        public string SolicitarCompra([FromServices]PedidoDAO pedidoDAO, [FromServices]FornecedoresService fornecedoresService, int id)
        {
            var pedido = pedidoDAO.BuscarPedido(id);

            if (pedido is null)
                return "Pedido não encontrado";

            pedido.IdLojaNoFornecedor = Constants.IdLojaVirtualFornecedores.ToString();

            return fornecedoresService.SolicitarCompra(pedido);
        }

    }
}