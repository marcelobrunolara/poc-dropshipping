﻿using LojaVirtualWebApi.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace LojaVirtualWebAPi.Services
{
    public class FornecedoresService
    {
        public string SolicitarCompra(PedidoModel pedido)
        {

            try
            {
                using (var client = new HttpClient())
                {

                    var url = string.Format("http://localhost:8222?fornecedor={0}", pedido.IdFornecedor);

                    var pedidojson = JsonConvert.SerializeObject(pedido);

                    var content = new StringContent(pedidojson, Encoding.UTF8, "application/json");

                    var result = client.PostAsync(url, content).Result;

                    var responseContent = result.Content.ReadAsStringAsync().Result ?? string.Empty;

                    return responseContent;
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return e.Message;
            }
        }
    }
}
