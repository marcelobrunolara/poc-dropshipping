﻿using LojaVirtualWebAPi.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace LojaVirtualWebAPi.Services
{
    public class LogisticaService
    {
        public HistoricoStatusEntregaModel BuscarUltimoStatus(int pedidoId)
        {
            var statusEntrega = new HistoricoStatusEntregaModel();

            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://localhost:58604");
                    var url = string.Format(@"api/logistica/situacaopedido/{0}", pedidoId);

                    var result = client.GetStringAsync(url).Result;

                    if (string.IsNullOrEmpty(result))
                        return statusEntrega;

                    statusEntrega = JsonConvert.DeserializeObject<HistoricoStatusEntregaModel>(result);
                }
                return statusEntrega;
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
                return statusEntrega;
            }
        }
    }
}
