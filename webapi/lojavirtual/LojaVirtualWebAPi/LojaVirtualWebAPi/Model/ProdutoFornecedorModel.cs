﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LojaVirtualWebApi.Model
{
    public class ProdutoFornecedorModel
    {
        public int Id { get; set; }
        public int IdProduto { get; set; }
        public int IdFornecedor { get; set; }
        public double Preco { get; set; }
        public string IdProdutoNoFornecedor { get; set; }

        #region Propriedades Compostas

        public string NomeFornecedor { get; set; }

        #endregion
    }
}
