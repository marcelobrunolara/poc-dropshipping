﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LojaVirtualWebApi.Model
{
    public class FornecedorModel
    {
        public int Id { get; set; }
        public string NomeFornecedor { get; set; }
        public string Endereco { get; set; }
        public string Telefone { get; set; }
    }
}
