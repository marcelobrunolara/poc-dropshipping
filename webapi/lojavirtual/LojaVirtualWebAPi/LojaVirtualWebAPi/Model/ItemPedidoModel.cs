﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LojaVirtualWebApi.Model
{
    public class ItemPedidoModel
    {
        public int Id { get; set; }
        public int IdProdutoFornecedor { get; set; }
        public double Preco { get; set; }
        public int IdPedido { get; set; }

        #region Propriedades Compostas

        public string NomeProduto { get;set; }
        public string NomeFornecedor { get; set; }
        public string IdProdutoNoFornecedor { get; set; }

        #endregion
    }
}
