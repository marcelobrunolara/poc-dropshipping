﻿using LojaVirtualWebAPi.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LojaVirtualWebApi.Model
{
    public class PedidoModel
    {
        public int Id { get; set; }
        public int IdCliente { get; set; }
        public double PrecoTotal { get; set; }
        public DateTime DataPedido { get; set; }
        public int OrigemPedido { get; set; }
        public int IdVendedor { get; set; }
        public int IdFornecedor { get; set; }
        public string IdLojaNoFornecedor { get; set; }

        #region Propriedades Compostas
        public string DescricaoOrigem { get; set; }
        public IEnumerable<ItemPedidoModel> ItemsPedido { get; set; }
        public string DescricaoStatusEntrega { get; set; }
        public HistoricoStatusEntregaModel UltimoStatusEntrega {get;set;}

        #endregion
    }
}
