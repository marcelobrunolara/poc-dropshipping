﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LojaVirtualWebApi.Model
{
    public class StatusEntregaModel
    {
        public int Id { get; set; }
        public string Descricao { get; set; }
    }
}
