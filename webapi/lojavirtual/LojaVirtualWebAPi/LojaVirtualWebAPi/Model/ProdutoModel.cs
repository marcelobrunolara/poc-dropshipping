﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LojaVirtualWebApi.Model
{
    public class ProdutoModel
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Descricao { get; set; }
        public int CodigoCategoria { get; set; }

        #region Propriedades compostas

        public string DescricaoCategoria { get; set; }
        public IEnumerable<ProdutoFornecedorModel> ProdutoFornecedor { get; set; }

        #endregion
    }
}
