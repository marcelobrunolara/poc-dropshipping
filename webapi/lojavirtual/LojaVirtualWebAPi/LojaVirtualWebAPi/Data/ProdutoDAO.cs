﻿using LojaVirtualWebApi.Model;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Data.SqlClient;
using Dapper;

namespace LojaVirtualWebAPi.Data
{
    public class ProdutoDAO
    {

        public IConfiguration _configuracoes;

        public ProdutoDAO(IConfiguration configuracoes)
        {
            _configuracoes = configuracoes;
        }

        public IEnumerable<ProdutoModel> ListarTodos()
        {
             IEnumerable<ProdutoModel> produtos;
            using (SqlConnection conexao = new SqlConnection(
                _configuracoes.GetConnectionString("LojaVirtual")))
            {
                produtos = conexao.Query<ProdutoModel>(
                    @"SELECT p.*, c.Descricao as  DescricaoCategoria
                        FROM Produto p
                        JOIN Categoria c
                        ON p.CodigoCategoria = c.Id");

                foreach(var produto in produtos)
                {
                    var produtoFornecedor = conexao.Query<ProdutoFornecedorModel>(
                        @"SELECT pf.*, f.NomeFornecedor
                            FROM ProdutoFornecedor pf
                            JOIN Fornecedor f
                            ON pf.IdFornecedor = f.Id
                            WHERE IdProduto = @IdProduto", new { IdProduto = produto.Id });

                    produto.ProdutoFornecedor = produtoFornecedor;
                }
            }

            return produtos;
        }

        public ProdutoModel BuscarProdutoPorId(int id)
        {
            ProdutoModel produto;

            using (SqlConnection conexao = new SqlConnection(
                _configuracoes.GetConnectionString("LojaVirtual")))
            {

            produto = conexao.QueryFirstOrDefault<ProdutoModel>(
                    @"SELECT p.*, c.Descricao as  DescricaoCategoria
                        FROM Produto p
                        JOIN Categoria c
                        ON p.CodigoCategoria = c.Id
                        WHERE p.Id = @IdProduto", new { IdProduto = id});

            if (produto != null)
            {
                var produtoFornecedor = conexao.Query<ProdutoFornecedorModel>(
                    @"SELECT pf.*, f.NomeFornecedor
                FROM ProdutoFornecedor pf
                JOIN Fornecedor f
                ON pf.IdFornecedor = f.Id
                WHERE IdProduto = @IdProduto", new { IdProduto = produto.Id });

                produto.ProdutoFornecedor = produtoFornecedor;
            }
            }
            return produto;
        }

    }
}
