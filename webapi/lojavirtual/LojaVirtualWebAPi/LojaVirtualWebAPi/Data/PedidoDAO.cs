﻿using Dapper;
using LojaVirtualWebApi.Model;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace LojaVirtualWebAPi.Data
{
    public class PedidoDAO
    {
        public IConfiguration _configuracoes;

        public PedidoDAO(IConfiguration configuracoes)
        {
            _configuracoes = configuracoes;
        }

        public IEnumerable<PedidoModel> ListarTodos()
        {
            IEnumerable<PedidoModel> pedidos;
            using (SqlConnection conexao = new SqlConnection(
                _configuracoes.GetConnectionString("LojaVirtual")))
            {
                pedidos = conexao.Query<PedidoModel>(
                    @"SELECT p.*, 
                        op.Descricao as DescricaoOrigem 
                        FROM Pedido p 
                        JOIN OrigemPedido op
                        ON p.OrigemPedido = op.Id");

                foreach (var pedido in pedidos)
                {
                    var itemsPedido = conexao.Query<ItemPedidoModel>(
                        @"SELECT ip.*, fr.NomeFornecedor, pr.Nome as NomeProduto,
                            pf.IdProdutoNoFornecedor as IdProdutoNoFornecedor
                            FROM ItemPedido ip
                            JOIN ProdutoFornecedor pf 
                            ON ip.IdProdutoFornecedor = pf.Id
                            JOIN Fornecedor fr
                            ON fr.Id = pf.IdFornecedor
                            JOIN Produto pr     
                            ON pr.Id = pf.IdProduto
                            WHERE ip.IdPedido=@IdPedido", 
                        new { IdPedido = pedido.Id });

                    pedido.ItemsPedido = itemsPedido;
                }
            }

            return pedidos;
        }

        public PedidoModel BuscarPedido(int id)
        {
            PedidoModel pedido;
            using (SqlConnection conexao = new SqlConnection(
                _configuracoes.GetConnectionString("LojaVirtual")))
            {
                pedido = conexao.QueryFirstOrDefault<PedidoModel>(
                    @"SELECT p.*, 
                        op.Descricao as DescricaoOrigem 
                        FROM Pedido p 
                        JOIN OrigemPedido op
                        ON p.OrigemPedido = op.Id
                        WHERE p.Id = @Id", new { Id = id});

                    var itemsPedido = conexao.Query<ItemPedidoModel>(
                        @"SELECT ip.*, fr.NomeFornecedor, pr.Nome as NomeProduto,
                            pf.IdProdutoNoFornecedor as IdProdutoNoFornecedor
                            FROM ItemPedido ip
                            JOIN ProdutoFornecedor pf 
                            ON ip.IdProdutoFornecedor = pf.Id
                            JOIN Fornecedor fr
                            ON fr.Id = pf.IdFornecedor
                            JOIN Produto pr     
                            ON pr.Id = pf.IdProduto
                            WHERE ip.IdPedido=@IdPedido",
                        new { IdPedido = pedido.Id });

                    pedido.ItemsPedido = itemsPedido;
                }
            return pedido;
        }

        public string BuscarDescricaoStatusEntrega(int statusId)
        {
            using (SqlConnection conexao = new SqlConnection(
                _configuracoes.GetConnectionString("LojaVirtual")))
            {
                var descricao = conexao.QueryFirstOrDefault<string>(
                    @"SELECT Descricao
                        FROM StatusEntrega
                        WHERE id = @Id", new { Id = statusId });

                return descricao;
            }
        }

    }
    
}
