﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UsuarioWebApi.Models
{
    public class UsuarioModel
    {
        public long Id { get; set; }
        public string Email { get; set; }
        public string Nome { get; set; }
        public string Senha { get; set; }
        public short CodigoTipoUsuario { get; set; }
        public string Telefone { get; set; }

        //Propriedades Compostas
        public string DescricaoTipo { get; set; }
    }
}
