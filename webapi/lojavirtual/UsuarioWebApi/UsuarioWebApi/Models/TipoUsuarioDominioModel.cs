﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UsuarioWebApi.Models
{
    public class TipoUsuarioDominioModel
    { 
        public short Codigo { get; set; }
        public string Descricao { get; set; }
    }
}
