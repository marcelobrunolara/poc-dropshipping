﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using UsuarioWebApi.Data;
using UsuarioWebApi.Models;

namespace UsuarioWebApi.Controllers
{
    
    [Route("api/[controller]")]
    [ApiController]
    public class UsuarioController : ControllerBase
    {

        // GET api/values
        [HttpGet()]
        public IEnumerable<UsuarioModel> Get([FromServices]UsuarioDAO usuarioDAO)
        {
            var usuarios = usuarioDAO.RecuperarTodos();
            return usuarios;
        }

        // GET api/id
        [HttpGet("{id}")]
        public UsuarioModel Get([FromServices]UsuarioDAO usuarioDAO, int id)
        {
            var usuario = usuarioDAO.RecuperarUsuario(id);
            return usuario;
        }

        [HttpGet("Check")]
        public ActionResult<string> Get()
        {
            return "API Usuário está online";
        }
    }
}