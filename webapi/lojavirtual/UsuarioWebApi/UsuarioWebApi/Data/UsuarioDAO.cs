﻿using Dapper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using UsuarioWebApi.Models;

namespace UsuarioWebApi.Data
{
    public class UsuarioDAO
    {
        public IConfiguration _configuracoes;

        public UsuarioDAO(IConfiguration configuracoes)
        {
            _configuracoes = configuracoes;
        }

        public IEnumerable<UsuarioModel> RecuperarTodos()
        {
            using (SqlConnection conexao = new SqlConnection(
                _configuracoes.GetConnectionString("UsuarioDb")))
            {
                return conexao.Query<UsuarioModel>(
                    @"SELECT u.*, t.Descricao AS DescricaoTipo 
                        FROM Usuario u 
                        JOIN TipoUsuarioDominio t
                        ON u.CodigoTipoUsuario = t.Id ");
            }
        }

        public UsuarioModel RecuperarUsuario(int id)
        {
            using (SqlConnection conexao = new SqlConnection(
                _configuracoes.GetConnectionString("UsuarioDb")))
            {
                return conexao.QueryFirstOrDefault<UsuarioModel>(
                    @"SELECT u.*, t.Descricao AS DescricaoTipo 
                        FROM Usuario u 
                        JOIN TipoUsuarioDominio t
                        ON u.CodigoTipoUsuario = t.Id
                        WHERE u.Id = @Id", new { Id = id});
            }
        }
    }
}
