﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using Microsoft.Extensions.Options;
using System.Text;
using Newtonsoft.Json;
using LogisticaWebApi.Model;
using LogisticaWebApi.Data;

namespace LogisticaWebApi.Mensageria
{
    public class MQFactory
    {
        private  IConfiguration _configuration;
        private  RabbitMqConfigurations rabbitMQConfigurations;
        private  ConnectionFactory factory;

        public MQFactory()
        {
            try
            {
                var builder = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile($"appsettings.json");
                _configuration = builder.Build();

                rabbitMQConfigurations = new RabbitMqConfigurations();
                new ConfigureFromConfigurationOptions<RabbitMqConfigurations>(
                    _configuration.GetSection("RabbitMQConfigurations"))
                        .Configure(rabbitMQConfigurations);

                factory = new ConnectionFactory()
                {
                    HostName = rabbitMQConfigurations.HostName,
                    Port = rabbitMQConfigurations.Port,
                    UserName = rabbitMQConfigurations.UserName,
                    Password = rabbitMQConfigurations.Password
                };
            }
            catch (Exception ex)
            {
                var a = ex;
                
            }
        }

        public void Register()
        {
                var connection = factory.CreateConnection();
                var channel = connection.CreateModel();

                channel.QueueDeclare(queue: "FilaAlteracaoEntrega",
                                     durable: false,
                                     exclusive: false,
                                     autoDelete: false,
                                     arguments: null);

                var consumer = new EventingBasicConsumer(channel);
                consumer.Received += AlterarStatusEntrega;
                channel.BasicConsume(queue: "FilaAlteracaoEntrega",
                     autoAck: true,
                     consumer: consumer);
        }

        private  void AlterarStatusEntrega( object sender, BasicDeliverEventArgs e)
        {
            try
            {
                var json = Encoding.UTF8.GetString(e.Body);
                var historicoStatusEntregaModel = JsonConvert.DeserializeObject<HistoricoStatusEntregaModel>(json);

                if (_configuration is null)
                    return;

                var logisticaDAO = new LogisticaDAO(_configuration);

                int resultado = logisticaDAO.InserirStatusAtual(historicoStatusEntregaModel);
                if (resultado > 0)
                {
                    Console.WriteLine($"Novo status do pedido {historicoStatusEntregaModel.IdPedido} gravado" +
                        $"com sucesso.");
                    NotificarEnvioEmail(historicoStatusEntregaModel);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Novo status do pedido {ex.Message}");
            }
        }

        private void NotificarEnvioEmail(HistoricoStatusEntregaModel historicoStatusEntregaModel)
        {
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.QueueDeclare(queue: "FilaEnvioEmailUsuario",
                                     durable: false,
                                     exclusive: false,
                                     autoDelete: false,
                                     arguments: null);

                string message = JsonConvert.SerializeObject(historicoStatusEntregaModel);
                var body = Encoding.UTF8.GetBytes(message);

                channel.BasicPublish(exchange: "",
                                     routingKey: "FilaEnvioEmailUsuario",
                                     basicProperties: null,
                                     body: body);
            }

            Console.WriteLine("Informação logistica encaminhada com sucesso.");
        }
    }
}
