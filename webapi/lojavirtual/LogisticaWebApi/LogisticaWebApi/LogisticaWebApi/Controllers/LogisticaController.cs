﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LogisticaWebApi.Data;
using LogisticaWebApi.Model;
using Microsoft.AspNetCore.Mvc;
using LogisticaWebApi.Mensageria;

namespace LogisticaWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LogisticaController : ControllerBase
    {
        [HttpGet]
        public IEnumerable<HistoricoStatusEntregaModel> Get([FromServices]LogisticaDAO logisticaDAO)
        {
            var historicoEntrega = logisticaDAO.RecuperarTodos();
            return historicoEntrega;
        }

        [HttpGet("SituacaoPedido/{id}")]
        public HistoricoStatusEntregaModel Get([FromServices]LogisticaDAO logisticaDAO, int id)
        {
            var statusAtualPedido = logisticaDAO.RecuperarStatusAtualPedido(id);
            return statusAtualPedido;
        }

        // POST api/values
        [HttpPost]
        public bool Post([FromServices]LogisticaDAO logisticaDAO, [FromBody] HistoricoStatusEntregaModel bodyValue)
        {
            return logisticaDAO.InserirStatusAtual(bodyValue) > 0 ? true : false;
        }


        [HttpGet("Check")]
        public ActionResult<string> Get([FromServices]MQFactory mqFactory)
        {
            mqFactory.Register();
            return "API Logística está online";
        }
    }
}
