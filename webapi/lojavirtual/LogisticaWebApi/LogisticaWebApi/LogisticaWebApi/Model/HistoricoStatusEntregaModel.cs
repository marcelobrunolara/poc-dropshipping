﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace LogisticaWebApi.Model
{
    public class HistoricoStatusEntregaModel 
    {
        public int Id { get; set; }
        public int IdPedido { get; set; }
        public int StatusAtual { get; set; }
        public  int IdStatusAnterior { get; set; }
        public string NomeTransportadora { get; set; }
        public string CodigoRastreio { get; set; }
        public string UrlRastreio { get; set; }
        public DateTime DataModificacao { get; set; }
        public string Observacao { get; set; }
    }
}