﻿using Dapper;
using LogisticaWebApi.Model;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace LogisticaWebApi.Data
{
    public class LogisticaDAO
    {
        public IConfiguration _configuracoes;

        public LogisticaDAO(IConfiguration configuracoes)
        {
            _configuracoes = configuracoes;
        }

        public IEnumerable<HistoricoStatusEntregaModel> RecuperarTodos()
        {
            using (SqlConnection conexao = new SqlConnection(
                _configuracoes.GetConnectionString("LogisticaDb")))
            {
                return conexao.Query<HistoricoStatusEntregaModel>(
                    @"SELECT * FROM HistoricoStatusEntrega");
            }
        }

        public HistoricoStatusEntregaModel RecuperarStatusAtualPedido(int idPedido)
        {
            using (SqlConnection conexao = new SqlConnection(
                _configuracoes.GetConnectionString("LogisticaDb")))
            {
                return conexao.QueryFirstOrDefault<HistoricoStatusEntregaModel>(
                    @"SELECT * 
                      FROM HistoricoStatusEntrega
                      WHERE IdPedido = @Id
                      ORDER by DataModificacao DESC", new { Id = idPedido});
            }
        }

        public int InserirStatusAtual(HistoricoStatusEntregaModel novoStatusModel)
        {
            var statusAtualModel = RecuperarStatusAtualPedido(novoStatusModel.IdPedido);

            using (SqlConnection conexao = new SqlConnection(
            _configuracoes.GetConnectionString("LogisticaDb")))
            {

                return conexao.Execute(
                    @"INSERT INTO HistoricoStatusEntrega 
                      (IdPedido, StatusAtual, IdStatusAnterior, NomeTransportadora, Observacao, UrlRastreio, DataModificacao)
                     VALUES
                     (@IdPedido, @StatusAtual, @IdStatusAnterior, @NomeTransportadora, @Observacao, @UrlRastreio, GETDATE())",
                    new 
                    {
                        novoStatusModel.IdPedido,
                        novoStatusModel.StatusAtual,
                        IdStatusAnterior = statusAtualModel?.StatusAtual,
                        novoStatusModel.NomeTransportadora,
                        novoStatusModel.Observacao,
                        novoStatusModel.UrlRastreio
                    });
            }
        }
    }
}
