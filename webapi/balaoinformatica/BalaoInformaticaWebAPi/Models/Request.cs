﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BalaoInformaticaWebAPi.Models
{
    [Table("Request")]
    public class Request
    {
        public int Id { get; set; }
        public double TotalPrice { get; set; }
        public string RequestDate { get; set; }
        public string UserId { get; set; }
        [Computed]
        public IEnumerable<RequestItem> RequestItems { get; set; }
    }
}
