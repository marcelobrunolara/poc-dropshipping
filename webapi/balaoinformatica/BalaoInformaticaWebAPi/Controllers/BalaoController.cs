﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BalaoInformaticaWebAPi.DataAccess;
using BalaoInformaticaWebAPi.Models;
using Microsoft.AspNetCore.Mvc;

namespace BalaoInformaticaWebAPi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BalaoController : ControllerBase
    {
        // GET api/values
        [HttpGet]
        public IEnumerable<Request> Get([FromServices]RequestDAO requestDAO)
        {
            return requestDAO.GetAll();
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public Request Get([FromServices]RequestDAO requestDAO, int id)
        {
            return requestDAO.GetSpecific(id);
        }

        // POST api/values
        [HttpPost]
        public long Post([FromServices]RequestDAO requestDAO, [FromBody]Request value)
        {
            return requestDAO.Insert(value);
        }

        // GET api/values
        [HttpGet("Check")]
        public ActionResult<string> GetHealth()
        {
            return "Ballon API is online";
        }
    }
}
