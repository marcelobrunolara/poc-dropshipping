﻿using BalaoInformaticaWebAPi.Models;
using Dapper;
using Dapper.Contrib.Extensions;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace BalaoInformaticaWebAPi.DataAccess
{
    public class RequestDAO
    {
        public IConfiguration _configuracoes;

        public RequestDAO(IConfiguration configuracoes)
        {
            _configuracoes = configuracoes;
        }

        public IEnumerable<Request> GetAll()
        {
            using (SqlConnection conexao = new SqlConnection(
                _configuracoes.GetConnectionString("BalaoDb")))
            {
                var requests = conexao.Query<Request>(
                    @"SELECT * FROM Request");

                foreach(var request in requests)
                {
                    var requestItems = conexao.Query<RequestItem>(
                        @"SELECT * 
                            FROM RequestItem
                            WHERE RequestId = @id", new { id = request.Id});

                    request.RequestItems = requestItems;
                }

                return requests;
            }
        }

        public Request GetSpecific(int requestId)
        {
            using (SqlConnection conexao = new SqlConnection(
                _configuracoes.GetConnectionString("BalaoDb")))
            {
                var request = conexao.QueryFirstOrDefault<Request>(
                    @"SELECT * 
                        FROM Request 
                        WHERE Id = @Id", new { Id = requestId});

                if (request is null)
                    return null;

                var requestItems = conexao.Query<RequestItem>(
                    @"SELECT * 
                        FROM RequestItem
                        WHERE RequestId = @RequestId", new { RequestId = requestId});

                request.RequestItems = requestItems;
                return request;

            }           
        }

        public long Insert(Request request)
        {
            using (SqlConnection conexao = new SqlConnection(
                _configuracoes.GetConnectionString("BalaoDb")))
            {
                var requestId = conexao.Insert(request);

                if (request?.RequestItems is null)
                    return 0;

                foreach (var requestItem in request.RequestItems)
                {
                        conexao.Execute(
                        @"INSERT INTO RequestItem (RequestId, ProductId) 
                        VALUES (@RequestId, @Product)",
                        new
                        {
                            RequestId = requestId,
                            Product = requestItem.ProductId,
                        });
                }

                return requestId;
            }
        }



    }
}
