﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace ModuloEnvioEmail
{
    class Program
    {
        static void Main(string[] args)
        {

            var factory = new ConnectionFactory()
            {
                HostName = "localhost",
                Port = 5672,
                UserName = "guest",
                Password = "guest"
            };

            var connection = factory.CreateConnection();
            var channel = connection.CreateModel();

                channel.QueueDeclare(queue: "FilaEnvioEmailUsuario",
                                     durable: false,
                                     exclusive: false,
                                     autoDelete: false,
                                     arguments: null);

                var consumer = new EventingBasicConsumer(channel);
                consumer.Received += BuscarUsuarioEEnviarMail;
                channel.BasicConsume(queue: "FilaEnvioEmailUsuario",
                     autoAck: true,
                     consumer: consumer);


            Console.WriteLine("Aguardando mensagens para processamento");
            Console.WriteLine("Pressione uma tecla para encerrar...");
            Console.ReadKey();
        }

        private static void BuscarUsuarioEEnviarMail(object sender, BasicDeliverEventArgs e)
        {
            Console.WriteLine("Email enviado para usuário.");
        }
    }
}
