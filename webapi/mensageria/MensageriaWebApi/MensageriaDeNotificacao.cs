﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MensageriaWebApi
{
    public class MensageriaDeNotificacao
    {
        private static IConfiguration _configuration;

        public static void Inicializar()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile($"appsettings.json");
            _configuration = builder.Build();

            var rabbitMQConfigurations = new RabbitMqConfigurations();
            new ConfigureFromConfigurationOptions<RabbitMqConfigurations>(
                _configuration.GetSection("RabbitMQConfigurations"))
                    .Configure(rabbitMQConfigurations);

            var factory = new ConnectionFactory()
            {
                HostName = rabbitMQConfigurations.HostName,
                Port = rabbitMQConfigurations.Port,
                UserName = rabbitMQConfigurations.UserName,
                Password = rabbitMQConfigurations.Password
            };

            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.QueueDeclare(queue: "FilaEnvioEmailUsuario",
                                     durable: false,
                                     exclusive: false,
                                     autoDelete: false,
                                     arguments: null);

                var consumer = new EventingBasicConsumer(channel);
                consumer.Received += EnviarEmailParaUsuario;
                channel.BasicConsume(queue: "FilaEnvioEmailUsuario",
                     autoAck: true,
                     consumer: consumer);
            }
        }

        private static void EnviarEmailParaUsuario(object sender, BasicDeliverEventArgs e)
        {
            try
            {
                var json = Encoding.UTF8.GetString(e.Body);
                var historicoStatusEntregaModel = JsonConvert.DeserializeObject<StatusLogisticaModel>(json);

            }
            catch (Exception ex)
            {
                Console.WriteLine($"Novo status do pedido {ex.Message}");
            }
        }
    }
}
