﻿
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RabbitMQ.Client;

namespace MensageriaWebApi.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class InformarLogisticaController : ControllerBase
    {
        // POST api/values
        [HttpPost]
        public string Post([FromBody] StatusLogisticaModel value,
                        [FromServices]RabbitMqConfigurations rabbitMQConfigurations)
        {

            var factory = new ConnectionFactory()
            {
                HostName = rabbitMQConfigurations.HostName,
                Port = rabbitMQConfigurations.Port,
                UserName = rabbitMQConfigurations.UserName,
                Password = rabbitMQConfigurations.Password
            };

            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.QueueDeclare(queue: "FilaAlteracaoEntrega",
                                     durable: false,
                                     exclusive: false,
                                     autoDelete: false,
                                     arguments: null);

                string message = JsonConvert.SerializeObject(value);
                var body = Encoding.UTF8.GetBytes(message);

                channel.BasicPublish(exchange: "",
                                     routingKey: "FilaAlteracaoEntrega",
                                     basicProperties: null,
                                     body: body);
            }

            return "Informação logistica encaminhada com sucesso.";
        }

        [HttpGet]
        public string Get()
        {
            return "API Mensageria está online";
        }
    }
}
