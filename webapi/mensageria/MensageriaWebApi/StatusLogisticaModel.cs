﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MensageriaWebApi
{
    public class StatusLogisticaModel
    {
            public int IdPedido { get; set; }
            public int StatusAtual { get; set; }
            public string NomeTransportadora { get; set; }
            public string CodigoRastreio { get; set; }
            public string UrlRastreio { get; set; }
            public DateTime DataModificacao { get; set; }
            public string Observacao { get; set; }
    }
}
