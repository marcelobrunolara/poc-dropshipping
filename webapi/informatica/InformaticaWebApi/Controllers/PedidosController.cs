﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InformaticaWebApi.Modelo;
using Microsoft.AspNetCore.Mvc;

namespace InformaticaWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PedidosController : ControllerBase
    {
        // GET api/values
        [HttpGet]
        public IEnumerable<PedidoCliente> Get([FromServices]PedidoClienteDAO pedidoDAO)
        {
            return pedidoDAO.GetAll();
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public PedidoCliente Get([FromServices]PedidoClienteDAO pedidoDAO, int id)
        {
            return pedidoDAO.GetSpecific(id);
        }

        // POST api/values
        [HttpPost]
        public long Post([FromServices]PedidoClienteDAO requestDAO, [FromBody]PedidoCliente pedido)
        {
            return requestDAO.Insert(pedido);
        }

        // GET api/values
        [HttpGet("check")]
        public ActionResult<string> GetHealth()
        {
            return "API Informatica está online.";
        }
    }
}
