﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dapper.Contrib;
using Dapper.Contrib.Extensions;

namespace InformaticaWebApi.Modelo
{
    [Table("PedidoCliente")]
    public class PedidoCliente
    {
        public int Id { get; set; }
        public string IdCliente { get; set; }
        public double Total { get; set; }
        public DateTime Data { get; set; }

        [Computed]
        public IEnumerable<ItemPedidoCliente> ItemPedidoCliente { get; set; }
    }
}
