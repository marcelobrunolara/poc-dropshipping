﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InformaticaWebApi.Modelo
{
    [Table("ItemPedidoCliente")]
    public class ItemPedidoCliente
    {
        public int Id { get; set; }
        public int IdPedidoCliente { get; set; }
        public int IdProduto { get; set; }
        public double PrecoProduto { get; set; }

    }
}
