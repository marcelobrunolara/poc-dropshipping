﻿using Dapper;
using Dapper.Contrib.Extensions;
using InformaticaWebApi.Modelo;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace InformaticaWebApi
{
    public class PedidoClienteDAO
    {
        public IConfiguration _configuracoes;

        public PedidoClienteDAO(IConfiguration configuracoes)
        {
            _configuracoes = configuracoes;
        }

        public IEnumerable<PedidoCliente> GetAll()
        {
            using (SqlConnection conexao = new SqlConnection(
                _configuracoes.GetConnectionString("InformaticaDb")))
            {
                var pedidos = conexao.Query<PedidoCliente>(
                    @"SELECT * FROM PedidoCliente");

                foreach (var pedido in pedidos)
                {
                    var requestItems = conexao.Query<ItemPedidoCliente>(
                        @"SELECT * 
                            FROM ItemPedidoCliente
                            WHERE IdPedidoCliente = @id", new { id = pedido.Id });

                    pedido.ItemPedidoCliente = requestItems;
                }

                return pedidos;
            }
        }

        public PedidoCliente GetSpecific(int requestId)
        {
            using (SqlConnection conexao = new SqlConnection(
                _configuracoes.GetConnectionString("InformaticaDb")))
            {
                var pedido = conexao.QueryFirstOrDefault<PedidoCliente>(
                    @"SELECT * 
                        FROM PedidoCliente 
                        WHERE Id = @Id", new { Id = requestId });

                if (pedido is null)
                    return null;

                var requestItems = conexao.Query<ItemPedidoCliente>(
                    @"SELECT * 
                        FROM ItemPedidoCliente
                        WHERE IdPedidoCliente = @RequestId", new { RequestId = requestId });

                pedido.ItemPedidoCliente = requestItems;

                return pedido;

            }
        }

        public long Insert(PedidoCliente request)
        {
            using (SqlConnection conexao = new SqlConnection(
                _configuracoes.GetConnectionString("InformaticaDb")))
            {
                var idPedido = conexao.Insert(request);

                if (request?.ItemPedidoCliente is null)
                    return 0;

                foreach (var requestItem in request.ItemPedidoCliente)
                {
                    conexao.Execute(
                    @"INSERT INTO ItemPedidoCliente (IdPedidoCliente, IdProduto, PrecoProduto) 
                        VALUES (@IdPedidoCliente, @Produto, @Preco)",
                    new
                    {
                        IdPedidoCliente = idPedido,
                        Produto = requestItem.IdProduto,
                        Preco = requestItem.PrecoProduto
                    });
                }

                return idPedido;
            }
        }
    }
}
